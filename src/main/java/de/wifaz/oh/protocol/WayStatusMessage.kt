package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

class WayStatusMessage @JsonCreator constructor (
        @JsonProperty("referenceWayId") val wayId : String,
        @JsonProperty("status") val status: Way.Status

) : HitchMessage(wayId), java.io.Serializable {
}
