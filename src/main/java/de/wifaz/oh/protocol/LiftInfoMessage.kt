package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.ArrayList

class LiftInfoMessage @JsonCreator constructor (
        @JsonProperty("referenceWayId") referenceWayId : String,
        @JsonProperty("liftInfos") val liftInfos : ArrayList<LiftInfo>
): HitchMessage(referenceWayId), java.io.Serializable {}
