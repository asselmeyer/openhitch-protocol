package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

data class OHPoint @JsonCreator constructor  (
        @JsonProperty("latitude") val latitude: Double,
        @JsonProperty("longitude") val longitude: Double
) : java.io.Serializable {
    var description: String? = null

    override fun toString(): String {
        return description ?: String.format("(%.5f,%.5f)", latitude, longitude)
    }

    companion object {
        public fun fromLngLat(longitude: Double, latitude: Double): OHPoint {
            val result = OHPoint(latitude,longitude)
            return result
        }
    }
}
