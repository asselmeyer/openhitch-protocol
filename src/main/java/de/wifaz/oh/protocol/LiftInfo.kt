package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

data class LiftInfo @JsonCreator constructor (
        @JsonProperty("lift") val lift: Lift,
        @JsonProperty("role") val role: Role,
        @JsonProperty("partner") val partner: User,
        @JsonProperty("partnerWay") val partnerWay: Way
) : java.io.Serializable
