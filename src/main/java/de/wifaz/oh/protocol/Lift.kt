package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal

data class Lift @JsonCreator constructor (
        @JsonProperty("id") val id: String,
        @JsonProperty("status") val status: Lift.Status,
        @JsonProperty("passengerWayId") val passengerWayId: String,
        @JsonProperty("driverWayId") val driverWayId: String,
        @JsonProperty("pickUpPoint") val pickUpPoint: OHPoint,
        @JsonProperty("pickUpHint") val pickUpHint: BigDecimal,
        @JsonProperty("dropOffPoint") val dropOffPoint: OHPoint,
        @JsonProperty("dropOffHint") val dropOffHint: BigDecimal,
        @JsonProperty("usefulness") val usefulness: Double,
        @JsonProperty("sharedDistance") val sharedDistance: Double,
        @JsonProperty("pickupTime") val pickupTime: Long,
        @JsonProperty("price") val price: BigDecimal,
        @JsonProperty("currency") val currency: String,
        @JsonProperty("deviationDistance") val deviationDistance: Double,
        @JsonProperty("deviationTime") val deviationTime: Long
) : java.io.Serializable {
    enum class Status {
        SUGGESTED,
        REQUESTED,
        ACCEPTED,
        REJECTED,
        STARTED,
        FINISHED,
        DRIVER_CANCELED,
        PASSENGER_CANCELED
    }
}
