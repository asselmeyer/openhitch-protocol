package de.wifaz.oh.protocol

import retrofit2.http.*
import retrofit2.Call

import java.util.ArrayList

interface HitchInterface {

    @GET("monitorInfo")
    fun monitorInfo() : Call<MonitorInfo>

    @PUT("ways/create")
    fun createWay(@Body data: Way): Call<Void>

    @DELETE("way/{id}")
    fun deleteWay(@Path("id") id: String): Call<Void>

    // FIXME should be PATCH
    @PUT("lift/{id}/status")
    fun updateLiftStatus(@Path("id") id: String, @Body data: Lift.Status): Call<Void>

    // FIXME should be PATCH
    @PUT("way/{id}/status")
    fun updateWayStatus(@Path("id") id: String, @Body data: Way.Status): Call<Void>

    companion object {
        const val MAX_OFFERS = 10
    }

}
