package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo

// cf https://stackoverflow.com/questions/30362446/deserialize-json-with-jackson-into-polymorphic-types-a-complete-example-is-giv#30386694

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY)
@JsonSubTypes(JsonSubTypes.Type(value = LiftInfoMessage::class, name = "LiftInfoMessage"), JsonSubTypes.Type(value = LiftStatusMessage::class, name = "LiftStatusMessage"), JsonSubTypes.Type(value = LiftRemoveMessage::class, name = "LiftRemoveMessage"))
open class HitchMessage (
    val referenceWayId: String
): java.io.Serializable {
}
