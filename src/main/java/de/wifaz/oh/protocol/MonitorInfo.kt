package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.ArrayList

data class MonitorInfo @JsonCreator constructor (
        @JsonProperty("ways") val ways: ArrayList<Way>,
        @JsonProperty( "lifts") val lifts: ArrayList<Lift>
) : java.io.Serializable {
}
