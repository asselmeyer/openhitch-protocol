package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

class LiftRemoveMessage @JsonCreator constructor (
        @JsonProperty("referenceWayId") referenceWayId : String,
        @JsonProperty("liftId") val liftId: String
): HitchMessage(referenceWayId), java.io.Serializable {
}
