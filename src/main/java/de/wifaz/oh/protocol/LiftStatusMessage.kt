package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.ArrayList

class LiftStatusMessage @JsonCreator constructor (
        @JsonProperty("referenceWayId") referenceWayId : String,
        @JsonProperty("liftId") val liftId: String,
        @JsonProperty("status") val status: Lift.Status

) : HitchMessage(referenceWayId), java.io.Serializable {
}
