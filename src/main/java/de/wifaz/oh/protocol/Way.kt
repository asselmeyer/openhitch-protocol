package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class Way @JsonCreator constructor (
        @JsonProperty("id") val id: String,
        @JsonProperty("status") val status: Status,
        @JsonProperty("userId") val userId: String,
        @JsonProperty("role") val role: Role,
        @JsonProperty("waypoints") val waypoints: ArrayList<OHPoint>,
        @JsonProperty("startTime") val startTime: Long,
        @JsonProperty("endTime") val endTime: Long,
        @JsonProperty("seats") val seats: Int,
        @JsonProperty("autocommit") val autocommit: Boolean
) : java.io.Serializable {
    enum class Status {
        PRELIMINARY,  // client internal use only
        RESEARCH,  // searching on server for potential partners without publishing
        PUBLISHED,
        STARTED,
        FINISHED,
        CANCELED
    }
    companion object {
        final val bindingStates = EnumSet.of(Status.PUBLISHED, Status.STARTED)
    }
}
