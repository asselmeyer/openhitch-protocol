package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.annotation.ObjectIdGenerators

data class User @JsonCreator constructor (
        @JsonProperty("id") val id: String,
        @JsonProperty("firstName") val firstName: String,
        @JsonProperty("lastName") val lastName: String,
        @JsonProperty("nickName") val nickName: String,
        @JsonProperty("phone") val phone: String?
) : java.io.Serializable {
}
